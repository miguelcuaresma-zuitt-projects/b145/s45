const coursesData = [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. A eum quaerat, enim quibusdam placeat, laboriosam quam architecto praesentium sunt harum sit aliquam, vitae porro accusantium atque tempore excepturi quisquam eveniet?",
		price: 45000,
		onOffer: true,
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. A eum quaerat, enim quibusdam placeat, laboriosam quam architecto praesentium sunt harum sit aliquam, vitae porro accusantium atque tempore excepturi quisquam eveniet?",
		price: 50000,
		onOffer: true,
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. A eum quaerat, enim quibusdam placeat, laboriosam quam architecto praesentium sunt harum sit aliquam, vitae porro accusantium atque tempore excepturi quisquam eveniet?",
		price: 55000,
		onOffer: true,
	}	
]

export default coursesData;