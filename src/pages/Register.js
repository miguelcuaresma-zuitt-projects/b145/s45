import { Fragment, useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'

export default function Register(){

	const { user } = useContext(UserContext);
	
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNo, setMobileNo] = useState('')

	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('');
	const [isActive, setIsActive] = useState(false);
	

	function registerUser(e){
		e.preventDefault();

		fetch('http://localhost:4000/users/register', {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true){
				Swal.fire({
					title: "Email already exists",
					icon: "error",
					text: "Please provide another email"
				})
			} else {
				fetch('http://localhost:4000/users/register', {
					method: "POST",
					headers: {
						"Content-Type" : "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo,
						email: email,
						password1: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					if(data === true){
						setFirstName('')
						setLastName('')
						setMobileNo('')
						setEmail('')
						setPassword1('')

						Swal.fire({
							title: "Regitration Succesful",
							icon: "success",
							text: "Welcome to Zuitt!"
						})
					}  else {
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again"
						})
					}
				})
			}
		})

		// Clear input fields
		setFirstName('')
		setLastName('')
		setMobileNo('')
		setEmail('');
		setPassword1('');
	}

	useEffect(() => {
		if(firstName !== '' && lastName!== '' && mobileNo !== '' && email !== '' && password1 !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, mobileNo, email, password1]);

	return (

		(user.id) ? 
			<Navigate to="/courses"/>
			:
			<Fragment>
				<h1>Register</h1>
					<Form onSubmit={(e) => registerUser(e)}>
					<Form.Group controlId="userEmail">
						<Form.Label>Email Address</Form.Label>
						<Form.Control
							type="email"
							placeholder="Enter your email here"
							value={email}
							onChange={e => setEmail(e.target.value)}
							required
						/>
						<Form.Text className="text-muted">
							We will never share your email with anyone else.
						</Form.Text>
					</Form.Group>
					<Form.Group controlId="userFirstName">
						<Form.Label>First Name</Form.Label>
						<Form.Control
							type="text"
							placeholder="First Name"
							value={firstName}
							onChange={e => setFirstName(e.target.value)}
							required
						/>
					</Form.Group>
					<Form.Group controlId="userLastName">
						<Form.Label>Last Name</Form.Label>
						<Form.Control
							type="text"
							placeholder="Last Name"
							value={lastName}
							onChange={e => setLastName(e.target.value)}
							required
						/>
					</Form.Group>
					<Form.Group controlId="userMobileNo">
						<Form.Label>Mobile No</Form.Label>
						<Form.Control
							type="number"
							placeholder="Mobile No"
							value={mobileNo}
							onChange={e => setMobileNo(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group controlId="password1">
						<Form.Label>Password</Form.Label>
						<Form.Control
							type="password"
							placeholder="Enter your password here"
							value={password1}
							onChange={e => setPassword1(e.target.value)}
							required
						/>
					</Form.Group>

					{ isActive ? 
						<Button variant="success" type="submit" id="submitBtn" className="mt-3">Submit</Button>
						: 
						<Button variant="danger" type="submit" id="submitBtn" className="mt-3 mb-3" disabled>Submit</Button>
					}
				</Form>
			</Fragment>
		
	)
}