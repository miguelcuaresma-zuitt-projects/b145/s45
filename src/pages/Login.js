import { useState, useEffect, useContext, Fragment } from 'react';
import { Navigate } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import Swal  from 'sweetalert2';
import UserContext from '../UserContext';



export default function Login () {
	
	const { user, setUser } = useContext(UserContext);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false)

	function loginUser(e){

		e.preventDefault();

		fetch('https://localhost:4000/users/login',
			{
				method: 'POST',
				headers: {
					'Content-Type' : 'application/json'
				},
				body: JSON.stringify({
					email: email,
					password: password,
				})
			}
		)
		.then(res => res.json())
		.then(data => {

			console.log(data);
			if(typeof data.access !== 'undefined'){
				localStorage.setItem('token', data.access )
				retrieveUserDetails(data.access)

				Swal.fire({
					title: 'Login successful',
					icon: 'success',
					text: 'Welcome to Zuitt'
				})
			} else {

				Swal.fire({
					title: 'Authentication failed',
					icon: 'error',
					text: 'Check details and try again'
				})
			}
		});

		// localStorage.setItem('email', email)
		// localStorage.setItem('key', value)

		// setUser({
		// 	email: localStorage.getItem('email')
		// })

		setEmail('');
		setPassword('')

	}

	const retrieveUserDetails = (token) => {
		fetch('https://calm-lowlands-22295.herokuapp.com/users/details', {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() => {
		if(email !== '' && password !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])

	return (

		(user.id !== null) ?
			<Navigate to="/courses"/>
		:

		<Fragment>
			<h1>Login</h1>
			<Form onSubmit={(e) => loginUser(e)}>
				<Form.Group controlId="loginEmail">
					<Form.Label>Email</Form.Label>
					<Form.Control
						type="email"
						placeholder="email@mail.com"
						value={email}
						onChange={e => setEmail(e.target.value)}
						required
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Enter Password"
						value={password}
						onChange={e => setPassword(e.target.value)}
						required
					/>
				</Form.Group>
				{ isActive ?
					<Button variant="success" type="submit" id="submitBtn" className="mt-3">Submit</Button>
					:
					<Button variant="danger" type="submit" disabled className="mt-3 mb-3">Submit</Button>
				}
			</Form>
		</Fragment>
	)
}