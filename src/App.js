//dependencies
  import { useState, useEffect } from 'react';
  import { Container } from 'react-bootstrap';
  import { BrowserRouter as Router} from 'react-router-dom';
  import { Routes, Route } from 'react-router-dom';

//components
  import Home from './pages/Home';
  import MyNav from './components/Navbar';
  import Register from './pages/Register';
  import Login from './pages/Login';
  import Logout from './pages/Logout'
  import Courses from './pages/Courses';
  import Error from './pages/Error'
  import CourseView from './pages/CourseView'
  import './App.css';
  import { UserProvider } from './UserContext'

//App
  function App() {

    const [user, setUser] = useState({
      id: null,
      isAdmin: null
    });

    const unsetUser = () => {
      localStorage.clear()
    };

    useEffect(() => {
      // console.log(user)
      // console.log(localStorage)
      fetch('https://calm-lowlands-22295.herokuapp.com/users/details',
      {
        method: "POST",
        header: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(res => res.json())
      .then(data => {
        console.log(data)
        if(typeof data._id !== 'undefined'){
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
          })
        } else {
          setUser({
            id: null,
            isAdmin: null,
          })
        }
      })
    }, [])


    return (
      <UserProvider value ={{user, setUser, unsetUser}}>
      <Router>
        <MyNav/>
        <Container>
        <Routes>
          <Route exact path="/" element={<Home/>}/>
          <Route exact path="/courses" element={<Courses/>}/><Route exact path="/courses/:courseId" element={<CourseView/>}/>

          <Route exact path="/register" element={<Register/>}/>
          <Route exact path="/login" element={<Login/>}/>
          <Route exact path="/logout" element={<Logout/>}/>
          <Route exact path="*" element={<Error/>}/>
        </Routes>
        </Container>               
      </Router>
      </UserProvider>
    );
  };

export default App;
