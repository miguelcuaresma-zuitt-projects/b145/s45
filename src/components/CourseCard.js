import { Row, Card } from 'react-bootstrap';
import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom';


export default function CourseCard({courseProp}) {
	const {name, description, price, _id} = courseProp;

// Syntax
	// const [getter, setter] = useState(initialValueGetter)
	// const [count, setCount] = useState(0);
	// const [seats, setSeat] = useState(30)

	// function enroll() {
			
	// 	if(seats > 0) {

	// 		setSeat(seats - 1);
	// 		setCount(count + 1);
	// 		console.log(`Enrollees: ${count}`);
	// 		console.log(seats);
	// 	}
		
	// }
	// 	useEffect(() => {
	// 		if (seats === 0) {
	// 			alert('No more seats available');
	// 		}
	// 	}, [seats]);

	return (
		<Row>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
						<h2>{name}</h2>
						</Card.Title>
						<Card.Text className="mb-0">
							Description:
						</Card.Text>	
						<Card.Text>
								{description}
						</Card.Text>									
						<Card.Text className="mb-0">
							Price:
						</Card.Text>
						<Card.Text>
							{price}								
						</Card.Text>
						<Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
					</Card.Body>
				</Card>
		</Row>
	);
};